LINTER=./node_modules/.bin/eslint
NAME=juardian


zip: lint
	cd addon; zip ../${NAME}.zip *

lint:
	${LINTER} addon/*.js
	! grep browser addon/*.js
	# grep '"use strict";' addon/*.js > /dev/null checked by jshint
	python2 -m json.tool addon/manifest.json > /dev/null
	python2 -m json.tool addon/preset.json > /dev/null
	python2 -m json.tool addon/schema.json > /dev/null
	tidy -eq addon/options.html
	tidy -eq addon/blockpage.html
	./meta/check_schema_equals_preset.sh


cp: zip
	cp ${NAME}.zip /tmp
	cd /tmp && unzip -o ${NAME}.zip
